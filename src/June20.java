/**
 * Created by Wait on 2016/6/20.
 */

import java.util.*;

public class June20 {
    public static void main(String[] args) {

        // Page 45. Ex. 5,6, dog bark.
        class Dog {
            String name, says;
        }
        Dog spot = new Dog();
        Dog scruffy = new Dog();
        spot.name = "spot";
        spot.says = "Ruff!";
        scruffy.name = "scruffy";
        scruffy.says = "Wurf!";
        System.out.println(spot.name + " barks " + spot.says);
        System.out.println(scruffy.name + " barks " + scruffy.says);
        Dog spot_ii = new Dog();
        spot_ii = spot;
        System.out.println(spot == spot_ii);
        System.out.println(spot.equals(spot_ii));

        //P46. Ex. 7,coin flipping
        int count;
        Random coin = new Random();
        for (count = 2; count > 0; count--) {
            System.out.println(coin.nextInt(2));
        }

        // shortcircuit

        //toBinaryString()
        int i = -1;
        System.out.println(Integer.toBinaryString(i));
        System.out.println(Integer.toBinaryString((i >>= 10)));
        System.out.println(Integer.toBinaryString((i >>>= 10)));
        short s = -1;
        System.out.println(Integer.toBinaryString(s));
        System.out.println(Integer.toBinaryString((s >>>= 10)));
        long l = -1;
        System.out.println(Long.toBinaryString(l));
        System.out.println(Long.toBinaryString((l >>>= 10)));

        // Logical operators is too boring,however I have to remember it,but next time.
        // P49

        // ternary if-else operator;
        // boolean-exp ? value0 : value1;

        //: operators/StringOperators.java
        int x = 0, y = 1, z = 2;
        String s1 = "x, y, z ";
        System.out.println(s1 + x + y + z);
        System.out.println(x + " ." + s1);
        s1 += "(summed) = ";
        System.out.println(s1 + (x + y + z));
        System.out.println("" + x);

        // P63. Ex.14,
        class StringOps {
             public void ops(String s2, String s3) {
                 System.out.println(s2 == s3);
                 System.out.println(s2.equals(s3));
            }
        }
        StringOps stringOps = new StringOps();
        stringOps.ops("xx","yy");

        // Charpeter 4 Begin

        // Java doesn't allow to use a number as a boolean.

        // P66. Ex.1.
        pritn1TO100();

        // P66. Ex.2.
        print25random();
        print25random2();

        // P67. Ex.4
        isPrimeNum(10000);
    }

    // wrong...MENGBI
    // see http://bbs.csdn.net/topics/300174937
    static void isPrimeNum(int i){
        if(i<1)
            return;
        System.out.print("Prime number:");
        int n,j,k;
        for(n =1;n < i;n++){
            for(j=1;j<n;j++)
            {
                k = n % j;
                if(k == 0)
                    break;
                System.out.print(" " + n);
            }

        }

    }

    static  void print25random() {
        int i = 0;
        int[] great = new int[25];
        int[] low = new int[25];
        int[] equal = new int[25];
        int h = 0, j = 0, k = 0;
        int rand = (int) (Math.random() * 100);
        int next;
        do {
            next =(int) (Math.random() * 100);
            if (rand > next)
                great[h++] = rand;
            else if (rand < next)
                low[j++] = rand;
            else
                equal[k++] = rand;
            rand = next;
            i++;
        } while (i < 25);
        System.out.println(Arrays.toString(great));
        System.out.println(Arrays.toString(equal));
        System.out.println(Arrays.toString(low));
    }

    static  void print25random2() {
        int i = 0;
        int rand = (int) (Math.random() * 100);
        int next;
        do {
            next =(int) (Math.random() * 100);
            if (rand > next)
                System.out.println("Great");
            else if (rand < next)
                System.out.println("Low");
            else
                System.out.println("Equal");
            rand = next;
            i++;
        } while (i < 25);
    }


    static void pritn1TO100() {
        int i;
        for(i = 1;i < 101;i++)
            System.out.println(i);
    }
}
