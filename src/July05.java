/**
 * Created by wait on 16-7-5.
 */


import java.util.*;


public class July05 {

    public static void main(String args[]) {

        isEmptyStringOrNullString();

        // char is in UTF-16

        codePointInCJ("abcdefghijkmlnopqrstuvwxyz");

        aScannerTest();

        // TODO:CJ P60,3.7.3

    }

    public static void isEmptyStringOrNullString() {
        String str1 = "";
        if (str1.length() == 0)
            System.out.println("str1 is a empty string");
        if (str1.equals(""))
            System.out.println("str1 is a empty string");
        String str2 = null;
        if (str2 == null)
            System.out.println("str2 is a null string");
        String str3 = "a string";
        if (str3 != null && str3.length() != 0)
            System.out.println("str3 isn't null or empty string");
    }

    public static void codePointInCJ(String str4) {


        // [代码点(Code Point)和代码单元(Code Unit)](http://www.cnblogs.com/zhangzl419/archive/2013/05/21/3090601.html)
        int str4CodeUnitCount = str4.length();
        System.out.println("str4's Code Unit Count is " + str4CodeUnitCount);
        int str4CodePointCount = str4.codePointCount(0, str4.length());
        System.out.println("str4's Code Point Count is " + str4CodePointCount);
        for (int i1 = 0; i1 < str4CodeUnitCount; i1++) {
            char str4CodeUnit = str4.charAt(i1); // charAt() return Code Unit
            System.out.println(str4CodeUnit);
        }
        for (int i2 = 0; i2 < str4CodePointCount; i2++) {
            char str4CodeUnit = str4.charAt(i2); // charAt() return Code Unit
            System.out.println(str4CodeUnit);
        }
    }


    public static StringBuilder builder = new StringBuilder();

    public static void aScannerTest() {

        Scanner in = new Scanner(System.in);
        System.out.println("Input Name");
        String name = in.nextLine();
        builder.append(name);
        builder.append("'s age is ");
        System.out.println("Input age");
        int age = in.nextInt();
        builder.append(age);
        String str5 = builder.toString();
        System.out.println(str5);
    }



}
