/**
 * Created by Wait on 2016/6/21.
 */

import java.util.*;

public class June21 {
    public static void main(String[] args) {

//        // foreach
//        // for(int a : b) ;; a <== each b
//        foreachUse("Test foreach");
//
//        // Lable
//        // Loop must follow behind the lable.
//        // e.g.,break label1 breaks all the way out to lable1,but it doesn't reenter the iteration.
//        labeledFor();   //A labeled break drops out of the bottom of the end of the loop denoted by the label.
//
//        switchOps();
//
//        // P75,Ex.9
//        p75ex9(100);

        // Ex.10
//        p75ex10();
//        p75ex10v2();


    }

//    // Vampire number
//    // http://primerecords.dk/vampires/
//    static void p75ex10() {
//        int vampire, i, j, k, l;
//        for(i = 1; i < 10;i++)
//            for(j = 0;j < 10;j++)
//                for(k = 0;k < 10;k++)
//                    for(l =0;l < 10;l++) {
////                        vampire = i*1000 +j*100 + k*10 + l;
//                        // 我想的太复杂了，大爷的
//                    }
//    }
//    static void p75ex10v2() {
//        TODO
//    }


    //Fibonacci
    static void p75ex9(int i) {
        System.out.print("Fibonacci:");
        for(int n = 1,j = 0;j <= i;n++) {
            j += n;
            System.out.print(j + " ");
        }
    }
     static void switchOps() {
         Random rand = new Random(47);
         for (int i = 0; i < 2; i++) {
             int c = rand.nextInt(26) + 'a';
             System.out.println((char) c + ", " + c + ": ");
             switch (c) {
                 case 'a':
                 case 'e':
                 case 'i':
                 case 'o':
                 case 'u':
                     System.out.println("vowel");
                     break;
                 case 'y':
                 case 'w':
                     System.out.println("Sometimes a vowel");
                     break;
                 default:
                     System.out.println("consonant");
             }
         }
     }

    //: control/LabeledFor.java
    // For loops with "labeled break" and "labeled continue."
    static void labeledFor() {
        int i = 0;
        outer: // Can't have statements here
        for(; true ;) { // infinite loop
            inner: // Can't have statements here
            for(; i < 10; i++) {
                System.out.println("i = " + i);
                if(i == 2) {
                    System.out.println("continue");
                    continue;
                }
                if(i == 3) {
                    System.out.println("break");
                    i++; // Otherwise i never
                    // gets incremented.
                    break;
                }
                if(i == 7) {
                    System.out.println("continue outer");
                    i++; // Otherwise i never
                    // gets incremented.
                    continue outer;
                }
                if(i == 8) {
                    System.out.println("break outer");
                    break outer;
                }
                for(int k = 0; k < 5; k++) {
                    if(k == 3) {
                        System.out.println("continue inner");
                        continue inner;
                    }
                }
            }
        }
        // Can't break or continue to labels here
    }

    static void foreachUse(String args) {
        for(char a : args.toCharArray())
            System.out.print(a + " ");
    }
    
}
