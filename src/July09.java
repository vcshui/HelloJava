import java.util.Arrays;

/**
 * Created by wait on 16-7-9.
 */
public class July09 {

    public static void main(String[] args) {

        // array copy
        arrayCopyTest();

        // array sort
        arrayCopyTest2(6, 36);

        
    }

    public static void arrayCopyTest2(int draw, int number) {

        int[] numbers = new int[number];
        for (int i = 0; i < numbers.length; i++)
            numbers[i] = i + 1;

        int[] draws = new int[draw];
        for (int i = 0; i < draws.length; i++) {
//            draws[i] = (int) (Math.random() * n);
            draws[i]=numbers[((int) (Math.random() * number))];
            numbers[((int) (Math.random() * number))]=numbers[number-1];
            number--;
        }

        // array sort
        Arrays.sort(draws);

        // print result
        for(int p:draws)
            System.out.println(p);


    }

    public static void arrayCopyTest() {

        int[] array1 = {2, 3, 5, 7, 11, 13};
        System.out.println("array1 = " + Arrays.toString(array1));
        int[] array2 = array1;
        array2[3] = 6;
        System.out.println("array2 = " + Arrays.toString(array2));
        System.out.println("array1 = " + Arrays.toString(array1));
        int[] array3 = Arrays.copyOf(array1, array1.length);
        System.out.println("array3 = " + Arrays.toString(array3));
        array1 = Arrays.copyOf(array1, 2 * array1.length);
        System.out.println("array1 = " + Arrays.toString(array1));

    }
}
