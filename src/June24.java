/**
 * Created by Wait on 2016/6/24.
 * 参考文章
 * Java访问性与包的讲解
 * http://www.2cto.com/kf/201502/375600.html
 */

import java.util.*;

public class June24 {

    //   关键字的作用：
//   1.public关键字，这个好理解，声明主函数为public就是告诉其他的类可以访问这个函数。
//   2.static关键字，告知编译器main函数是一个静态函数。也就是说main函数中的代码是存储在静态存储区的，即当定义了类以后,
//     这段代码就已经存在了。如果main()方法没有使用static修饰符，那么编译不会出错，但是如果你试图执行该程序将会报错，
//     提示main()方法不存在。因为包含main()的类并没有实例化（即没有这个类的对象），所以其main()方法也不会存。
//     而使用static修饰符则表示该方法是静态的，不需要实例化即可使用。
//   3.void关键字表明main()的返回值是无类型。
//   4.参数String[] args，作用是为程序使用者在命令行状态下与程序交互。
    public static void main(String[] args) {

        // Each overloaded method must take a unique list of argument types.
        // Even differences in the ordering of arguments are sufficient to distinguish two methods.
//        treeInfo();


    }


    static void treeInfo() {
        for (int i = 0; i < 5; i++) {
            Tree t = new Tree(i);
            t.info();
            t.info("overloaded");
        }
    }
}

class Tree {
    int height;

    // no-arg constructor
    Tree() {
        System.out.println("Plant a seed.");
        height = 0;
    }

    Tree(int initialheight) {
        height = initialheight;
        System.out.println("Tree is " + height);
    }

    void info() {
        System.out.println("default");
    }

    ;

    void info(String s) {
        System.out.println("overloaded method");
    }
}
