import java.math.BigInteger;
import java.util.Arrays;

/**
 * Created by wait on 16-7-8.
 */


public class July08 {


    public static void main(String[] args) {

        lotteryOdds1(6, 36);
        lotteryOdds2(60, 360);

        arrayTest();
    }

    public static void arrayTest() {
        int[] a = new int[9];
//        for(int i = 1;i<=9;i++)
//            a[i]=i;
        int i = 0;
        do {
            a[i] = i++;
        } while (i < 9);
        for (int x : a)
            System.out.print(x);
        System.out.println();
        System.out.println(Arrays.toString(a));
    }

    public static void lotteryOdds1(int draw, int number) {

        System.out.println("LOTTERY NO.1 DRAW: " + draw);
        System.out.println("THE HIGHEST NUMBER: " + number);
        // compute binomial coefficient number*(number-1)*...*(number-draw+1)/(1*2*3*...*draw)
        long lotteryOdds = 1;
        for (int i = 1; i < draw; i++)
            lotteryOdds = lotteryOdds * (number - i + 1) / 1;
        System.out.println("Odds in LOTTERY NO.1 is 1/" + lotteryOdds);

    }

    public static void lotteryOdds2(int draw, int number) {

        System.out.println("LOTTERY NO.2 DRAW: " + draw);
        System.out.println("THE HIGHEST NUMBER: " + number);
        BigInteger lotteryOdds = BigInteger.valueOf(1);
        for (int i = 1; i < draw; i++)
            lotteryOdds = lotteryOdds.multiply(BigInteger.valueOf(number - i + 1)).divide(BigInteger.valueOf(1));
        System.out.println("Odds in LOTTERY NO.2 is 1/" + lotteryOdds);

    }
}
