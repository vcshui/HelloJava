import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.jar.Pack200;

/**
 * Created by wait on 16-7-10.
 */

public class July10 {

    public static void main(String[] args) {

        dateTest1();

        dateTest2();

    }

    public static void dateTest1() {

        System.out.println(new Date());

        // new的返回值是一个引用

        GregorianCalendar calendar = new GregorianCalendar(2016, Calendar.JULY, 10);
        Date day1 = calendar.getTime();

        GregorianCalendar calendar1 = new GregorianCalendar();
        calendar1.setTime(day1);
        int year = calendar1.get(Calendar.YEAR);
        System.out.println(year);

    }

    public static void dateTest2() {

//        Locale.setDefault(Locale.CHINA);

        // construct date as current date
        GregorianCalendar date = new GregorianCalendar();
        int today = date.get(Calendar.DAY_OF_MONTH);
        int month = date.get(Calendar.MONTH);

        // set date to start date of the month
        date.set(Calendar.DAY_OF_MONTH,1);
        int weekday = date.get(Calendar.DAY_OF_WEEK);

        int firstDayOfWeek = date.getFirstDayOfWeek();

        int indent = 0;
        while(weekday != firstDayOfWeek) {
            indent++;
            date.add(Calendar.DAY_OF_MONTH,-1);
        }

        // print weekday names
        String[] weekdayNames = new DateFormatSymbols().getShortWeekdays();
        do {
            System.out.printf("%4s",weekdayNames[weekday]);
            date.add(Calendar.DAY_OF_MONTH,1);
            weekday=date.get(Calendar.DAY_OF_WEEK);
        }while (weekday!=firstDayOfWeek);
        System.out.println();

    }
}
